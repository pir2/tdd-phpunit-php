<?php
require "phpunit/Calculator.php";
class CalculatorTest extends PHPUnit_Framework_TestCase
{
     /**
     * Generated from @assert (1, 1) == 2.
     */
    public function testAddSuccess()
    {
        $object = new Calculator();
        $this->assertEquals(2,$object->add(1, 1));
    }
    
}
?>