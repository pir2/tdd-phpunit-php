<?php

class Money {

    private $amount = 0;

    public function __construct($amount) {
        $this->amount = $amount;
    }

    public function deposit($amount) {
        if (is_numeric($amount)) {
            return $this->amount + $amount;
        } else {
            return false;
        }
    }

    public function withdraw($amount) {
        $result = "";
        if (!is_numeric($amount)) {
            return false;
        }

        if ($this->amount < $amount) {
            return false;
        } else {
            return $this->amount - $amount;
        }
    }

    public function getAmount() {
        return $this->amount;
    }

}
