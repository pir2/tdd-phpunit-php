<?php

require "phpunit/Money.php";

class MoneyTest extends PHPUnit_Framework_TestCase {

    // ...

    public function testDeposit() {
        // Arrange
        $a = new Money(1000);
        // Assert
        $this->assertEquals(1500, $a->deposit(500));
    }
    
    public function testDepositFailure() {
        // Arrange
        $a = new Money(1000);
        // Assert
        $this->assertEquals(false, $a->deposit("aaa"));
    }
    
    public function testWithdraw() {
        // Arrange
        $a = new Money(1000);
        // Assert
        $this->assertEquals(500, $a->withdraw(500));
    }
    
    public function testWithdrawFailure() {
        $a = new Money(1000);
        $this->assertEquals(false, $a->withdraw("xxx"));
    }

    public function testWithdrawNotEnough() {
        // Arrange
        $a = new Money(1000);
        $this->assertEquals(false, $a->withdraw(1500));
    }
}
